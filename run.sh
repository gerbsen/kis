#!/bin/bash

mkdir -p _data

curl "https://docs.google.com/spreadsheets/d/e/2PACX-1vReBbaQEqull9cIoxvCHuXdUGul-KPapOvjYPa7bWkWOYQdnKvYNW5IF-DZaN_brO3pQfx7XwraDGG1/pub?gid=1031444148&single=true&output=csv" -o _data/ai-companies.csv

# ruby scripts/csv2yml.rb _data/ai-companies.csv --json > _data/ai-companies.json
ruby scripts/csv2yml.rb _data/ai-companies.csv > _data/ai-companies.yml

rm _data/ai-companies.csv
# rm _data/ai-companies.json
bundle exec jekyll serve --trace